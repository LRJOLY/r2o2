# defines which global planner should be used
base_global_planner: global_planner/GlobalPlanner
# defines which local planner should be used
# base_local_planner: dwa_local_planner/DWAPlannerROS
base_local_planner: teb_local_planner/TebLocalPlannerROS
# the fixed frame name inside the created map
# global_frame: map

# the root of the robot model
robot_base_frame: base_footprint
clearing_rotation_allowed: false # allows clearing rotation recovery for default move_base
conservative_reset_dist: 0.1 # parameterization of recovery for default move_base
controller_frequency: 10.0 # frequency at which the local planner will search trajectories
controller_patience: 0.5 # how long the local planner will search before giving up and triggering the local one
max_planning_retries: 3 # how many times the global planner will try to find a path before performing recovery
oscillation_distance: 0.2 # How far in meters the robot must move to be considered not to be oscillating. Moving this far resets the timer counting up to the ~oscillation_timeout
oscillation_timeout: 10.0 # How long in seconds to allow for oscillation before executing recovery behaviors. A value of 0.0 corresponds to an infinite timeout.
planner_frequency: 0.5 # how often a new global plan should be created 
planner_patience: 0.5 # maximum duration for creating a plan
recovery_behavior_enabled: true # allow execution of recovery behaviors
restore_defaults: false # Automatic reset to defaults through dynamic reconfigure
shutdown_costmaps: false # should costmaps be shutdown when movebase is not running
recovery_behaviors: # List of recovery behaviors, used in original move_base 
  - {name: conservative_reset, type: clear_costmap_recovery/ClearCostmapRecovery}
conservative_reset: # Definition of the above recovery behavior
  reset_distance: 0.0 # The radius away from the robot in meters outside which obstacles will be removed from the costmaps when they are reverted to the static map, 0.0 means clear everything
  layer_names: [pointcloud_layer ] # impacted layers for the reset
clearing_radius: 1.5 # radius around the robot to be cleaned up , when clear costmap recovery is invoked, (only for default move_base)

#Parameters of the global planner
GlobalPlanner:
  cost_factor: 0.8 # multiplicator applied to costmap costs
  lethal_cost: 253 # lethal cost for a cell
  neutral_cost: 66 # neutral cost for a cell
  orientation_mode: 1 # prefers forward orientation on the path
  publish_potential: true # publish potential costs (for debugging)
  allow_unknown: true # allow global planning through unknown space  
  old_navfn_behavior: false # Exactly mirror behavior of navfn, use defaults for other boolean parameters, default false
  use_quadratic: true # Use the quadratic approximation of the potential. Otherwise, use a simpler calculation, default true
  use_dijkstra: true # Use dijkstra's algorithm. Otherwise, A*, default true
  use_grid_path: false # Create a path that follows the grid boundaries. Otherwise, use a gradient descent method, default false

TebLocalPlannerROS:
 odom_topic: /odom
    
 # Robot Configuration Parameters         
 max_vel_x: 1.0 # Maximum translational velocity of the robot in meters/sec
 max_vel_x_backwards: 0.2 # Maximum absolute translational velocity of the robot while driving backwards in meters/sec. See optimization parameter weight_kinematics_forward_drive
 max_vel_y: 1.0
 max_vel_theta: 0.3 # the angular velocity is also bounded by min_turning_radius in case of a carlike robot (r = v / omega)
 acc_lim_x: 0.6 # Maximum translational acceleration of the robot in meters/sec^2
 acc_lim_theta: 0.6 # Maximum angular acceleration of the robot in radians/sec^2
 min_turning_radius: 0.0 # Minimum turning radius of a carlike robot (set to zero for a diff-drive robot).

 footprint_model: # types: "point", "circular", "two_circles", "line", "polygon"
   type: "polygon"
   vertices: [[-0.25, -0.3], [-0.25, 0.3], [0.25, 0.3], [0.25, -0.3]] # for type "polygon"   

 # Goal Tolerance Parameters
 xy_goal_tolerance: 0.6 # Allowed final euclidean distance to the goal position in meters
 yaw_goal_tolerance: 0.25 # Allowed final orientation error in radians
 free_goal_vel: True # Remove the goal velocity constraint such that the robot can arrive at the goal with maximum speed

 # Trajectory Configuration Parameters
 teb_autosize: True #  Enable automatic resizing of the trajectory w.r.t to the temporal resolution (recommended)
 dt_ref: 0.3 # Desired temporal resolution of the trajectory (the trajectory is not fixed to dt_ref since the temporal resolution is part of the optimization, but the trajectory will be resized between iterations if dt_ref +-dt_hysteresis is violated.
 dt_hysteresis: 0.03 # Hysteresis for automatic resizing depending on the current temporal resolution, usually approx. 10% of dt_ref is recommended
 global_plan_overwrite_orientation: True # Overwrite orientation of local subgoals provided by the global planner (since they often provide only a 2D path)
 allow_init_with_backwards_motion: False # If true, underlying trajectories might be initialized with backwards motions in case the goal is behind the start within the local costmap (this is only recommended if the robot is equipped with rear sensors).
 max_global_plan_lookahead_dist: 5 # Specify the maximum length (cumulative Euclidean distances) of the subset of the global plan taken into account for optimization. The actual length is than determined by the logical conjunction of the local costmap size and this maximum bound. Set to zero or negative in order to deactivate this limitation.
 feasibility_check_no_poses: 5 # Specify up to which pose on the predicted plan the feasibility should be checked each sampling interval.
    
 # Obstacle Parameters
 min_obstacle_dist: 0.35 # Minimum desired separation from obstacles in meters
 inflation_dist: 0.5 # Buffer zone around obstacles with non-zero penalty costs (should be larger than min_obstacle_dist in order to take effect)
 dynamic_obstacle_inflation_dist: 0.45 # Buffer zone around predicted locations of dynamic obstacles with non-zero penalty costs (should be larger than min_obstacle_dist in order to take effect)
 include_dynamic_obstacles: True # Specify whether the movement of dynamic obstacles should be predicted by a constant velocity model (this also effects homotopy class planning); If false, all obstacles are considered to be static.
 include_costmap_obstacles: True # Specify if obstacles of the local costmap should be taken into account. Each cell that is marked as obstacle is considered as a point-obstacle. Therefore do not choose a very small resolution of the costmap since it increases computation time. In future releases this circumstance is going to be addressed as well as providing an additional api for dynamic obstacles.
 costmap_obstacles_behind_robot_dist: 1.5 # Limit the occupied local costmap obstacles taken into account for planning behind the robot (specify distance in meters).
 obstacle_poses_affected: 30 # Each obstacle position is attached to the closest pose on the trajectory in order to keep a distance. Additional neighbors can be taken into account as well. Note, this parameter might be removed in future versions, since the the obstacle association strategy has been modified in kinetic+. Refer to the parameter description of legacy_obstacle_association.
 costmap_converter_plugin: "" # Define plugin name in order to convert costmap cells to points/lines/polygons. Set an empty string to disable the conversion such that all cells are treated as point-obstacles.
 costmap_converter_spin_thread: True # If set to true, the costmap converter invokes its callback queue in a different thread.
 costmap_converter_rate: 5 # Rate that defines how often the costmap_converter plugin processes the current costmap (the value should not be much higher than the costmap update rate) [in Hz].
 
 # Optimization Parameters
 no_inner_iterations: 5 # Number of actual solver iterations called in each outerloop iteration. See param no_outer_iterations.
 no_outer_iterations: 4 # Each outerloop iteration automatically resizes the trajectory according to the desired temporal resolution dt_ref and invokes the internal optimizer (that performs no_inner_iterations). The total number of solver iterations in each planning cycle is therefore the product of both values.
 optimization_activate: True # Activate the optimization
 optimization_verbose: False # Print verbose information
 penalty_epsilon: 0.1 # Add a small safety margin to penalty functions for hard-constraint approximations
 weight_max_vel_x: 2 # Optimization weight for satisfying the maximum allowed translational velocity
 weight_max_vel_theta: 1 # Optimization weight for satisfying the maximum allowed angular velocity
 weight_acc_lim_x: 1 # Optimization weight for satisfying the maximum allowed translational acceleration
 weight_acc_lim_theta: 1 # Optimization weight for satisfying the maximum allowed angular acceleration
 weight_kinematics_nh: 1000 # Optimization weight for satisfying the non-holonomic kinematics (this parameter must be high since the kinematics equation constitutes an equality constraint, even a value of 1000 does not imply a bad matrix condition due to small 'raw' cost values in comparison to other costs).
 weight_kinematics_forward_drive: 1000 # Optimization weight for forcing the robot to choose only forward directions (positive transl. velocities). A small weight (e.g. 1.0) still allows driving backwards.
 weight_kinematics_turning_radius: 1 # Optimization weight for enforcing a minimum turning radius (only for carlike robots).
 weight_optimaltime: 1 # Optimization weight for contracting the trajectory w.r.t transition/execution time
 weight_obstacle: 1 # Optimization weight for keeping a minimum distance from obstacles
 weight_dynamic_obstacle: 20 # Optimization weight for satisfying a minimum separation from dynamic obstacles
 weight_dynamic_obstacle_inflation: 0.01 # Optimization weight for the inflation penalty of dynamic obstacles (should be small)
 weight_adapt_factor: 2 # Some special weights (currently weight_obstacle) are repeatedly scaled by this factor in each outer TEB iteration (weight_new = weight_old*factor). Increasing weights iteratively instead of setting a huge value a-priori leads to better numerical conditions of the underlying optimization problem.
 weight_viapoint: 1 #  Optimization weight for minimizing the distance to via-points
 weight_prefer_rotdir: 50 # Optimization weight for preferring a specific turning direction (-> currently only activated if an oscillation is detected, see 'oscillation_recovery'

 # Homotopy Class Planner : Parallel Planning in distinctive Topologies
 enable_homotopy_class_planning: True # Activate parallel planning in distinctive topologies (requires much more CPU resources, since multiple trajectories are optimized at once)
 enable_multithreading: True # Activate multiple threading in order to plan each trajectory in a different thread
 simple_exploration: False
 max_number_classes: 3 # Specify the maximum number of distinctive trajectories taken into account (limits computational effort)
 selection_cost_hysteresis: 1.0 # Specify how much trajectory cost must a new candidate have w.r.t. a previously selected trajectory in order to be selected (selection if new_cost < old_cost*factor).
 selection_obst_cost_scale: 1.0 # Extra scaling of obstacle cost terms just for selecting the 'best' candidate.
 selection_alternative_time_cost: False # If true, time cost (sum of squared time differences) is replaced by the total transition time (sum of time differences).
 roadmap_graph_no_samples: 15 # Specify the number of samples generated for creating the roadmap graph
 roadmap_graph_area_width: 5 # Random keypoints/waypoints are sampled in a rectangular region between start and goal. Specify the width of that region in meters.
 h_signature_prescaler: 0.5 # Scale internal parameter (H-signature) that is used to distinguish between homotopy classes. Warning: reduce this parameter only, if you observe problems with too many obstacles in the local cost map, do not choose it extremly low, otherwise obstacles cannot be distinguished from each other (0.2<value<=1).
 h_signature_threshold: 0.1 # Two H-signatures are assumed to be equal, if both the difference of real parts and complex parts are below the specified threshold.
 obstacle_keypoint_offset: 0.1
 obstacle_heading_threshold: 1.0  # Specify the value of the scalar product between obstacle heading and goal heading in order to take them (obstacles) into account for exploration.
 visualize_hc_graph: False # Visualize the graph that is created for exploring distinctive trajectories (check marker message in rviz)

 # Recovery : parameters related to recovery and backup strategies
 # shrink_horizon_backup: True # Allows the planner to shrink the horizon temporary (50%) in case of automatically detected issues.
 # shrink_horizon_min_duration: 10 # Specify minimum duration for the reduced horizon in case an infeasible trajectory is detected.
 # oscillation_recovery: True #  Try to detect and resolve oscillations between multiple solutions in the same equivalence class (robot frequently switches between left/right/forward/backwards)
 # oscillation_v_eps: 0.1 # Threshold for the average normalized linear velocity: if oscillation_v_eps and oscillation_omega_eps are not exceeded both, a possible oscillation is detected
 # oscillation_omega_eps: 0.1 # Threshold for the average normalized angular velocity: if oscillation_v_eps and oscillation_omega_eps are not exceeded both, a possible oscillation is detected
 # oscillation_recovery_min_duration: 0.2 # Minumum duration [sec] for which the recovery mode is activated after an oscillation is detected.
 # oscillation_filter_duration: 0.5 # Filter length/duration [sec] for the detection of oscillations
