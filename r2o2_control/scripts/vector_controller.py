#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64, Float64MultiArray
from math import sqrt, atan, sin, cos, pi, acos

class Vehicule():
    def __init__(self):
        # Vehicule characteristics
        self.width = 0.45 # vehicule width [m]
        self.length = 0.6 # vehicule length [m]
        self.wheel_radius = 0.2 #wheel radius [m]
        # calculated datas
        self.R = 0.5 * sqrt(self.width*self.width + self.length*self.length)
        self.alpha = atan(self.width / self.length)
        # suscriber(s)
        self.ros_sub_cmd_vel = rospy.Subscriber("/r2o2/cmd_vel", Twist, self.cb_cmd_vel)
        # publisher(s)
        self.ros_pub_legs_angle = rospy.Publisher("/r2o2_legs_controller/command", Float64MultiArray, queue_size=10)
        self.ros_pub_wheels_velocity = rospy.Publisher("/r2o2_wheels_controller/command", Float64MultiArray, queue_size=10)

    def cb_cmd_vel(self, msg):
        #print(msg)
        Vx = msg.linear.x
        Vy = msg.linear.y
        Wz = msg.angular.z
        
        Vzx = Wz * self.R * sin(self.alpha)
        Vzy = Wz * self.R * cos(self.alpha)
 
        fl_Vx = Vx - Vzx
        fl_Vy = Vy + Vzy
        fr_Vx = Vx + Vzx
        fr_Vy = Vy + Vzy
        rl_Vx = Vx - Vzx
        rl_Vy = Vy - Vzy
        rr_Vx = Vx + Vzx
        rr_Vy = Vy - Vzy

        Wwheels = []
        legs_angle = []
        angle, V = self.lawv_calculation(fr_Vx, fr_Vy)
        #rospy.loginfo("FR wheel : Vx =%s, Vy= %s, V=%s, angle = %s (%s)" %(fr_Vx, fr_Vy, V, angle, 180/pi*angle))
        legs_angle.append( angle )
        Wwheels.append( V/self.wheel_radius )
        angle, V = self.lawv_calculation(fl_Vx, fl_Vy)
        legs_angle.append( angle )
        #rospy.loginfo("FL wheel : Vx =%s, Vy= %s, V=%s, angle = %s (%s)" %(fl_Vx, fl_Vy, V, angle, 180/pi*angle))
        Wwheels.append( V/self.wheel_radius )
        angle, V = self.lawv_calculation(rr_Vx, rr_Vy)
        #rospy.loginfo("RR wheel : Vx =%s, Vy= %s, V=%s, angle = %s (%s)" %(rr_Vx, rr_Vy, V, angle, 180/pi*angle))
        legs_angle.append( angle )
        Wwheels.append( V/self.wheel_radius )
        angle, V = self.lawv_calculation(rl_Vx, rl_Vy)
        #rospy.loginfo("RL wheel : Vx =%s, Vy= %s, V=%s, angle = %s (%s)" %(rl_Vx, rl_Vy, V, angle, 180/pi*angle))
        legs_angle.append( angle )
        Wwheels.append( V/self.wheel_radius )

        legs_msg = Float64MultiArray(data=legs_angle)
        self.ros_pub_legs_angle.publish(legs_msg)
        wheels_msg = Float64MultiArray(data=Wwheels)
        self.ros_pub_wheels_velocity.publish(wheels_msg)

    def lawv_calculation(self, Vx, Vy):
        V = sqrt( Vx*Vx + Vy*Vy )
        if V == 0:
            angle = 0
        elif Vy < 0.0:
            angle = -acos( Vx/V )
        else:
            angle = acos( Vx/V )
        return angle, V


    def driver(self):
        rospy.spin()

if __name__ == "__main__":
    rospy.init_node('r2o2_driver')
    r2o2 = Vehicule()
    r2o2.driver()