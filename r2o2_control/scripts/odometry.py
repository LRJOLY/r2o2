#!/usr/bin/env python

import math
from math import sin, cos, pi, sqrt, pow

import rospy
import tf
from sensor_msgs.msg import JointState
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
from numpy import mean


class Odometry_Publisher():
    def __init__(self):
        # Robot datas
        self.wheel_radius = 0.2
        self.wheels_joints_name = ["fl_wheel_joint", "fr_wheel_joint", "rl_wheel_joint", "rr_wheel_joint"]
        self.legs_joints_name = ["fl_leg_joint", "fr_leg_joint", "rl_leg_joint", "rr_leg_joint"]
        self.wheels_number = len(self.wheels_joints_name)
        self.body_width = 0.45
        self.body_length = 0.6
        self.leg_radius = 0.5 * sqrt( pow(self.body_width, 2.0) + pow(self.body_length, 2) )
        self.SA = self.body_width / ( 2.0 * self.leg_radius )

        # Variables
        self.current_time = rospy.Time.now()
        self.last_time = rospy.Time.now()
        self.wheels_joints_index = []
        self.legs_joints_index = []
        self.x = 0.0
        self.y = 0.0
        self.th = 0.0
        self.vx = 0.1
        self.vy = -0.1
        self.vth = 0.1

        # ROS objects
        self.odom_broadcaster = tf.TransformBroadcaster()
        self.r = rospy.Rate(50.0)

        # Publishers
        self.ros_pub_odom = rospy.Publisher("odom", Odometry, queue_size=50)
        
        # Subscribers
        self.ros_sub_joint_states = rospy.Subscriber("/joint_states", JointState, self.cb_joint_states)

    def wheel_velocity_linear_components_calc(self, wheel_vel, theta):
        Vx = self.wheel_radius * wheel_vel * cos(theta)
        Vy = self.wheel_radius * wheel_vel * sin(theta)
        return Vx, Vy

    def robot_velocity_linear_components_calc(self, wheels_lin_vel):
        # wheels_lin_vel is a 4x2 list
        vx,vy = mean(wheels_lin_vel, axis=0)
        return vx, vy

    def cb_joint_states(self, msg):
        # if index are not known, we search them in the joint_states msg
        if self.wheels_joints_index == [] or self.legs_joints_index == []:
            joints_name = msg.name
            for name in self.wheels_joints_name:
                self.wheels_joints_index.append( joints_name.index( name ) )
            for name in self.legs_joints_name:
                self.legs_joints_index.append( joints_name.index( name ) )
        # get wheels velocities and legs angles
        wheels_vel = []
        for i in self.wheels_joints_index:
            wheels_vel.append( msg.velocity[i] )
        legs_angles = []
        for i in self.legs_joints_index:
            legs_angles.append( msg.position[i] )
        wheels_lin_vel = []
        for i in range(0, self.wheels_number):
            vx, vy = self.wheel_velocity_linear_components_calc( wheels_vel[i], legs_angles[i])
            wheels_lin_vel.append([vx, vy])
        # compute linear velocities of the robot (mean of tthe wheels veolicties)
        self.vx, self.vy = self.robot_velocity_linear_components_calc(wheels_lin_vel)
        # compute angular velocity of the robot
        wheels_ang_vel = []
        for i in range(0, self.wheels_number):
            wheels_ang_vel.append(pow(-1,i)*(wheels_lin_vel[i][0] - self.vx)/(self.leg_radius * self.SA))
        #DEBUG : print(wheels_ang_vel)
        self.vth = mean(wheels_ang_vel)
        #rospy.loginfo("vx : %s, vy: %s, vth: %s", self.vx, self.vy, self.vth)

    def odom_calc(self):
        self.current_time = rospy.Time.now()
        dt = (self.current_time - self.last_time).to_sec()
        delta_x = (self.vx * cos(self.th) - self.vy * sin(self.th)) * dt
        delta_y = (self.vx * sin(self.th) + self.vy * cos(self.th)) * dt
        delta_th = self.vth * dt

        self.x += delta_x
        self.y += delta_y
        self.th += delta_th

    def get_odom_msg(self):
        # quaternion creation (from yaw)
        odom_quat = tf.transformations.quaternion_from_euler(0, 0, self.th)
        # first, we'll publish the transform over tf
        self.odom_broadcaster.sendTransform(
            (self.x, self.y, 0.),
            odom_quat,
            self.current_time,
            "base_footprint",
            "odom"
        )
        # next, we'll publish the odometry message over ROS
        odom = Odometry()
        odom.header.stamp = self.current_time
        odom.header.frame_id = "odom"

        # set the position
        odom.pose.pose = Pose(Point(self.x, self.y, 0.), Quaternion(*odom_quat))
        # TO DO COVARIANCE

        # set the velocity
        odom.child_frame_id = "base_footprint"
        odom.twist.twist = Twist(Vector3(self.vx, self.vy, 0), Vector3(0, 0, self.vth))
        # TO DO COVARIANCE

        return odom

    def action(self):
        while not rospy.is_shutdown():

            self.odom_calc()
            # publish the message
            self.ros_pub_odom.publish(self.get_odom_msg())

            self.last_time = self.current_time
            self.r.sleep()


if __name__ == "__main__":
    rospy.init_node('odometry_publisher')
    odom_pub = Odometry_Publisher()
    odom_pub.action()
