#!/usr/bin/env python

import pygame, sys
import pygame.locals
import rospy
from geometry_msgs.msg import Twist

class Pad():
    def __init__(self):
        ## Parameters
        # Speed
        self.linear_increment = 0.1
        self.angular_increment = 0.05
        self.linear_max_speed = 1.0
        self.angular_max_speed = 0.5
        # Pygame
        self.BLACK = (0, 0, 0)
        self.WIDTH = 200
        self.HEIGHT = 100
        # Keys
        self.linear_key_inc_x = pygame.K_UP
        self.linear_key_dec_x = pygame.K_DOWN
        self.linear_key_inc_y = pygame.K_LEFT
        self.linear_key_dec_y = pygame.K_RIGHT
        self.angular_key_inc_z = pygame.K_PAGEDOWN
        self.angular_key_dec_z = pygame.K_PAGEUP
        self.stop_key = pygame.K_SPACE
        # Publishers
        self.ros_pub_twist_ = rospy.Publisher('/r2o2/cmd_vel', Twist, queue_size=1)
        
        ## Variables
        self.twist = Twist()
        self.twist.linear.x = 0.0
        self.twist.linear.y = 0.0
        self.twist.angular.z = 0.0

    def speed_control(self, type, speed_component, direction):
        if (type == "linear"):
            speed_limit = self.linear_max_speed
            speed_step = direction * self.linear_increment
        elif (type == "angular"):
            speed_limit = self.angular_max_speed
            speed_step = direction * self.angular_increment
        speed_component += speed_step
        if (abs(speed_component) > speed_limit):
            sign = lambda a: (a>0) - (a<0)
            speed_component = sign(speed_component) * speed_limit
        return speed_component

    def key_analysis(self):            
        # Opening of the PYGAME window
        pygame.init()
        windowSurface = pygame.display.set_mode((self.WIDTH, self.HEIGHT), 0, 32)
        windowSurface.fill(self.BLACK)
        # Definition of the acquisition frequency
        r = rospy.Rate(20)
        # analysis loop
        while not rospy.is_shutdown():
            event = pygame.event.wait()
            if event.type != 2:
                pass
            elif event.key == self.linear_key_inc_x:
                #increment speed along X axis
                self.twist.linear.x = self.speed_control("linear", self.twist.linear.x, 1.0)
            elif event.key == self.linear_key_dec_x:
                #decrement speed along X axis
                self.twist.linear.x = self.speed_control("linear", self.twist.linear.x, -1.0)
            elif event.key == self.linear_key_inc_y:
                #increment speed along Y axis
                self.twist.linear.y = self.speed_control("linear", self.twist.linear.y, 1.0)
            elif event.key == self.linear_key_dec_y:
                #decrement speed along Y axis
                self.twist.linear.y = self.speed_control("linear", self.twist.linear.y, -1.0)
            elif event.key == self.angular_key_inc_z:
                #increment speed around Z axis
                self.twist.angular.z = self.speed_control("angular", self.twist.angular.z, 1.0)
            elif event.key == self.angular_key_dec_z:
                #decrement speed around Z axis
                self.twist.angular.z = self.speed_control("angular", self.twist.angular.z, -1.0)
            elif event.key == self.stop_key:
                # stop 
                self.twist.linear.x = 0.0
                self.twist.linear.y = 0.0
                self.twist.angular.z = 0.0
            elif (event.key == pygame.K_x or event.key == pygame.K_y):
                self.active_axis = "linear"
                rospy.loginfo("Selection de l'increment de vitesse lineaire")
            elif event.key == pygame.K_z:
                self.active_axis = "angular"
                rospy.loginfo("Selection de l'increment de vitesse lineaire")
            elif (event.key == pygame.K_PLUS or event.key == pygame.K_KP_PLUS):
                if self.active_axis == "linear":
                    self.linear_increment = self.linear_increment * 2.0
                    rospy.loginfo("Nouvel increment : %s m/s" %self.linear_increment)
                elif self.active_axis == "angular":
                    self.angular_increment = self.angular_increment * 2.0
                    rospy.loginfo("Nouvel increment : %s rad/s" %self.angular_increment)
            elif (event.key == pygame.K_MINUS or event.key == pygame.K_KP_MINUS):
                if self.active_axis == "linear":
                    self.linear_increment = self.linear_increment / 2.0
                    rospy.loginfo("Nouvel increment : %s m/s" %self.linear_increment)
                elif self.active_axis == "angular":
                    self.angular_increment = self.angular_increment / 2.0
                    rospy.loginfo("Nouvel increment : %s rad/s" %self.angular_increment)
            elif event.key == pygame.K_DELETE:
                if self.active_axis == "linear":
                    self.linear_increment = 0.1
                    rospy.loginfo("Nouvel increment : %s m/s" %self.linear_increment)
                elif self.active_axis == "angular":
                    self.angular_increment = 0.05
                    rospy.loginfo("Nouvel increment : %s rad/s" %self.angular_increment)
            # Publication du twist genere
            self.ros_pub_twist_.publish(self.twist)
            #pass
            if event.type == pygame.locals.QUIT:
                pygame.quit()
                sys.exit()
        r.sleep()
        
if __name__ == '__main__':
    try :
        rospy.init_node('keyboard_teleop', anonymous=True)
        speed_pad = Pad()
        speed_pad.key_analysis()
    except rospy.ROSInterruptException:
        pass
