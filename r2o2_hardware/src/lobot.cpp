#include <r2o2_hardware/lobot.h>


LobotInterface::LobotInterface(const char *device)
{
  SetPort(device);
}

LobotInterface::~LobotInterface(){
  //DESTROYPTR();
  ROS_WARN("LobotInterface/Destructor - object killed");
}

void LobotInterface::SetPort(const char *device){
  // set driver port
  fd = open(device, O_RDWR | O_NOCTTY);
}

uint8_t LobotInterface::CheckSum(uint8_t buf[])
{
  uint8_t i;
  uint8_t temp = 0;
  for (i = 2; i < buf[3] + 2; i++) {
    temp += buf[i];
  }
  temp = ~temp;
  i = (uint8_t)temp;
  return i;
}

void LobotInterface::ServoMove(unsigned int id, int16_t position, uint16_t time)
{
  unsigned char buf[10];
  if(position < 0)
    position = 0;
  if(position > 1000)
    position = 1000;
  buf[0] = buf[1] = SERVO_FRAME_HEADER;
  buf[2] = id;
  buf[3] = 7;
  buf[4] = SERVO_MOVE_TIME_WRITE;
  buf[5] = GET_LOW_BYTE(position);
  buf[6] = GET_HIGH_BYTE(position);
  buf[7] = GET_LOW_BYTE(time);
  buf[8] = GET_HIGH_BYTE(time);
  buf[9] = CheckSum(buf);
  if (write(fd, buf, 10) < 0) perror("move servo position mode");
  if (tcdrain(fd) < 0) perror("move servo position mode tcdrain");
}

void LobotInterface::ServoStopMove(uint8_t id)
{
  unsigned char buf[6];
  buf[0] = buf[1] = SERVO_FRAME_HEADER;
  buf[2] = id;
  buf[3] = 3;
  buf[4] = SERVO_MOVE_STOP;
  buf[5] = CheckSum(buf);
  if (write(fd, buf, 6) < 0) perror("stop servo position mode");
  if (tcdrain(fd) < 0) perror("stop servo position mode tcdrain");
}

void LobotInterface::ServoSetID(uint8_t oldID, uint8_t newID)
{
  unsigned char buf[7];
  buf[0] = buf[1] = SERVO_FRAME_HEADER;
  buf[2] = oldID;
  buf[3] = 4;
  buf[4] = SERVO_ID_WRITE;
  buf[5] = newID;
  buf[6] = CheckSum(buf);
  if (write(fd, buf, 7) < 0) perror("set servo ID");
  if (tcdrain(fd) < 0) perror("set servo ID tcdrain");
  
  #ifdef DEBUG
    ROS_WARN("SERVO ID WRITING");
  #endif
}

void LobotInterface::ServoSetMode(uint8_t id, uint8_t mode, int16_t speed)
{
  unsigned char buf[10];

  buf[0] = buf[1] = SERVO_FRAME_HEADER;
  buf[2] = id;
  buf[3] = 7;
  buf[4] = SERVO_OR_MOTOR_MODE_WRITE;
  buf[5] = mode;
  buf[6] = 0;
  buf[7] = GET_LOW_BYTE((unsigned int)speed);
  buf[8] = GET_HIGH_BYTE((unsigned int)speed);
  buf[9] = CheckSum(buf);

  #ifdef SERVO_DEBUG
    ROS_WARN("SERVO Set Mode");
  #endif

  if (write(fd, buf, 10) < 0) perror("set servo mode");
  if (tcdrain(fd) < 0) perror("set servo mode tcdrain");
}

void LobotInterface::ServoLoad(uint8_t id)
{
  unsigned char buf[7];
  buf[0] = buf[1] = SERVO_FRAME_HEADER;
  buf[2] = id;
  buf[3] = 4;
  buf[4] = SERVO_LOAD_OR_UNLOAD_WRITE;
  buf[5] = 1;
  buf[6] = CheckSum(buf);
  
  if (write(fd, buf, 7) < 0) perror("servo load or unload");
  if (tcdrain(fd) < 0) perror("servo load or unload mode tcdrain");
  
  #ifdef SERVO_DEBUG
    ROS_WARN("LOBOT SERVO LOAD WRITE");
  #endif

}


int LobotInterface::ServoReadPosition(uint8_t id)
{
  int count = 10000;
  int ret;
  unsigned char buf[6];

  buf[0] = buf[1] = SERVO_FRAME_HEADER;
  buf[2] = id;
  buf[3] = 3;
  buf[4] = SERVO_POS_READ;
  buf[5] = CheckSum(buf);

  #ifdef SERVO_DEBUG
    ROS_WARN("LOBOT SERVO Pos READ");
  #endif

	if (write(fd, buf, 6) < 0) perror("command position reading");	// Write data to USB-ISS
	if (tcdrain(fd) < 0) perror("command position reading tcdrain");
	if (read(fd, buf, 6) < 0) perror("get the readed position");	// Read data back from USB-ISS, module ID and software version

  ret = (int16_t)BYTE_TO_HW(buf[2], buf[1]);

  #ifdef SERVO_DEBUG
    ROS_WARN(ret);
  #endif
  return ret;
}

int LobotInterface::ServoReadVin(uint8_t id)
{
  int count = 10000;
  int ret;
  unsigned char buf[6];

  buf[0] = buf[1] = SERVO_FRAME_HEADER;
  buf[2] = id;
  buf[3] = 3;
  buf[4] = SERVO_VIN_READ;
  buf[5] = CheckSum(buf);

  #ifdef SERVO_DEBUG
    ROS_WARN("LOBOT SERVO VIN READ");
  #endif

	if (write(fd, buf, 6) < 0) perror("command voltage reading");	// Write data to USB-ISS
	if (tcdrain(fd) < 0) perror("command voltage reading tcdrain");
	if (read(fd, buf, 6) < 0) perror("get the readed voltage");	// Read data back from USB-ISS, module ID and software version

  ret = (int16_t)BYTE_TO_HW(buf[2], buf[1]);

  #ifdef SERVO_DEBUG
    ROS_WARN(ret);
  #endif
  return ret;
}