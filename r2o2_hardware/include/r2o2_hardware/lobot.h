#pragma once

#include "stdio.h"
#include "stdlib.h"
#include <cstdint>
#include "unistd.h"
#include "termios.h"
#include "fcntl.h"
#include "ros/ros.h"
#include "sstream"

#define GET_LOW_BYTE(A) (uint8_t)((A))
#define GET_HIGH_BYTE(A) (uint8_t)((A) >> 8)
#define BYTE_TO_HW(A, B) ((((uint16_t)(A)) << 8) | (uint8_t)(B))

#define SERVO_FRAME_HEADER         0x55
#define SERVO_MOVE_TIME_WRITE      1
#define SERVO_MOVE_TIME_READ       2
#define SERVO_MOVE_TIME_WAIT_WRITE 7
#define SERVO_MOVE_TIME_WAIT_READ  8
#define SERVO_MOVE_START           11
#define SERVO_MOVE_STOP            12
#define SERVO_ID_WRITE             13
#define SERVO_ID_READ              14
#define SERVO_ANGLE_OFFSET_ADJUST  17
#define SERVO_ANGLE_OFFSET_WRITE   18
#define SERVO_ANGLE_OFFSET_READ    19
#define SERVO_ANGLE_LIMIT_WRITE    20
#define SERVO_ANGLE_LIMIT_READ     21
#define SERVO_VIN_LIMIT_WRITE      22
#define SERVO_VIN_LIMIT_READ       23
#define SERVO_TEMP_MAX_LIMIT_WRITE 24
#define SERVO_TEMP_MAX_LIMIT_READ  25
#define SERVO_TEMP_READ            26
#define SERVO_VIN_READ             27
#define SERVO_POS_READ             28
#define SERVO_OR_MOTOR_MODE_WRITE  29
#define SERVO_OR_MOTOR_MODE_READ   30
#define SERVO_LOAD_OR_UNLOAD_WRITE 31
#define SERVO_LOAD_OR_UNLOAD_READ  32
#define SERVO_LED_CTRL_WRITE       33
#define SERVO_LED_CTRL_READ        34
#define SERVO_LED_ERROR_WRITE      35
#define SERVO_LED_ERROR_READ       36

//#define SERVO_DEBUG 1


/**
 * This class makes some assumptions on the model of the robot:
 *  - the rotation axes of wheels are collinear
 *  - the wheels are identical in radius
 * Additional assumptions to not duplicate information readily available in the URDF:
 *  - the wheels have the same parent frame
 *  - a wheel collision geometry is a cylinder or sphere in the urdf
 *  - a wheel joint frame center's vertical projection on the floor must lie within the contact patch
 */
class LobotInterface
{
public:
  LobotInterface(const char *device);
  ~LobotInterface();

  /**
   * @brief move the servo (in position mode)
   * @param id id of the servo to move
   * @param position angle to be reached by the servo
   * @param time time to reached the target
   */
  void ServoMove(unsigned int id, int16_t position, uint16_t time);

  /**
   * @brief stop a servo
   * @param id id of the servo to stop
   */
  void ServoStopMove(uint8_t id);

  /**
   * @brief change servo ID
   * @param oldID old id of the servo
   * @param newID new id of the servo
   */
  void ServoSetID(uint8_t oldID, uint8_t newID);

  /**
   * @brief change mode (position or speed control)
   * @param id id of the servo to configure
   * @param mode Mode to be set (0 - position mode; 1 - speed mode)
   * @param speed speed (only for motor mode)
   */
  void ServoSetMode(uint8_t id, uint8_t mode, int16_t speed);

  /**
   * @brief check if the servo is powered or not
   * @param id id of the servo to check
   */
  void ServoLoad(uint8_t id);


  /**
   * @brief check if the servo is powered or not
   * @param device port to open
   */
  void SetPort(const char *device);


  /**
   * @brief read servo position
   * @param id servo ID
   * @return servo position
   */
  int ServoReadPosition(uint8_t id);

  /**
   * @brief read servo voltage
   * @param id servo ID
   * @return servo voltage
   */
  int ServoReadVin(uint8_t id);

private:
  /**
   * @brief check sum of the buffer
   * @param buf buffer
   * @return checksum
   */
  uint8_t CheckSum(uint8_t buf[]);

  int fd;

};
