cmake_minimum_required(VERSION 3.0.2)
project(r2o2_hardware)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
)

catkin_package(
  INCLUDE_DIRS include
#  LIBRARIES r2o2_hardware
#  CATKIN_DEPENDS other_catkin_pkg
#  DEPENDS system_lib
)

include_directories(
 include
 ${catkin_INCLUDE_DIRS}
)

add_executable(r2o2_hardware
    src/main.cpp
    #src/a390_better_hardware.cpp
    #src/motor_driver_controller.cpp
    src/lobot.cpp)
#add_dependencies(r2o2_hardware ${PROJECT_NAME}_generate_messages_cpp ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(r2o2_hardware ${catkin_LIBRARIES})

