#include <cmath>
#include <holonomic_controller/holonomic_controller.h>
#include <pluginlib/class_list_macros.hpp>
#include <tf/transform_datatypes.h>
#include <urdf/urdfdom_compatibility.h>
#include <urdf_parser/urdf_parser.h>

/*
* \brief Check that a link exists and has a geometry collision.
* \param link The link
* \return true if the link has a collision element with geometry
*/
static bool hasCollisionGeometry(const urdf::LinkConstSharedPtr& link)
{
  if (!link)
  {
    ROS_ERROR("Link pointer is null.");
    return false;
  }

  if (!link->collision)
  {
    ROS_ERROR_STREAM("Link " << link->name << " does not have collision description. Add collision description for link to urdf.");
    return false;
  }

  if (!link->collision->geometry)
  {
    ROS_ERROR_STREAM("Link " << link->name << " does not have collision geometry description. Add collision geometry description for link to urdf.");
    return false;
  }
  return true;
}

/*
 * \brief Check if the link is modeled as a cylinder
 * \param link Link
 * \return true if the link is modeled as a Cylinder; false otherwise
 */
static bool isCylinder(const urdf::LinkConstSharedPtr& link)
{
  if (!hasCollisionGeometry(link))
  {
    return false;
  }

  if (link->collision->geometry->type != urdf::Geometry::CYLINDER)
  {
    ROS_DEBUG_STREAM("Link " << link->name << " does not have cylinder geometry");
    return false;
  }

  return true;
}

/*
 * \brief Check if the link is modeled as a sphere
 * \param link Link
 * \return true if the link is modeled as a Sphere; false otherwise
 */
static bool isSphere(const urdf::LinkConstSharedPtr& link)
{
  if (!hasCollisionGeometry(link))
  {
    return false;
  }

  if (link->collision->geometry->type != urdf::Geometry::SPHERE)
  {
    ROS_DEBUG_STREAM("Link " << link->name << " does not have sphere geometry");
    return false;
  }

  return true;
}

/*
 * \brief Get the wheel radius
 * \param [in]  wheel_link   Wheel link
 * \param [out] wheel_radius Wheel radius [m]
 * \return true if the wheel radius was found; false otherwise
 */
static bool getWheelRadius(const urdf::LinkConstSharedPtr& wheel_link, double& wheel_radius)
{
  if (isCylinder(wheel_link))
  {
    wheel_radius = (static_cast<urdf::Cylinder*>(wheel_link->collision->geometry.get()))->radius;
    return true;
  }
  else if (isSphere(wheel_link))
  {
    wheel_radius = (static_cast<urdf::Sphere*>(wheel_link->collision->geometry.get()))->radius;
    return true;
  }

  ROS_ERROR_STREAM("Wheel link " << wheel_link->name << " is NOT modeled as a cylinder or sphere!");
  return false;
}

namespace holonomic_controller{

  HolonomicController::HolonomicController()
    : open_loop_(false)
    , command_struct_()
    , leg_desaxing_(0.0)
    , wheel_radius_(0.0)
    , leg_desaxing_multiplier_(1.0)
    , wheel_radius_multiplier_(1.0)
    , cmd_vel_timeout_(0.5)
    , allow_multiple_cmd_vel_publishers_(true)
    , base_frame_id_("base_link")
    , odom_frame_id_("odom")
    , enable_odom_tf_(true)
    , wheel_joints_size_(0)
    , publish_cmd_(false)
    , publish_wheel_joint_controller_state_(false)
  {
  }

  bool HolonomicController::init(hardware_interface::RobotHW* robot_hw,
            ros::NodeHandle& root_nh,
            ros::NodeHandle &controller_nh)
  {

    typedef hardware_interface::VelocityJointInterface VelIface;
    typedef hardware_interface::PositionJointInterface PosIface;
    typedef hardware_interface::JointStateInterface StateIface;

    // get multiple types of hardware_interface
    VelIface *vel_joint_if = robot_hw->get<VelIface>(); // vel for wheels
    PosIface *pos_joint_if = robot_hw->get<PosIface>(); // pos for steers


    const std::string complete_ns = controller_nh.getNamespace();
    std::size_t id = complete_ns.find_last_of("/");
    name_ = complete_ns.substr(id + 1);

    // Get joint names from the parameter server
    std::vector<std::string> wheel_names, leg_names;
    if (!getWheelAndLegNames(controller_nh, "front_left_wheel", wheel_names) ||
        !getWheelAndLegNames(controller_nh, "front_right_wheel", wheel_names) ||
        !getWheelAndLegNames(controller_nh, "rear_left_wheel", wheel_names) ||
        !getWheelAndLegNames(controller_nh, "rear_right_wheel", wheel_names) ||
        !getWheelAndLegNames(controller_nh, "front_left_leg", leg_names) ||
        !getWheelAndLegNames(controller_nh, "front_right_leg", leg_names) ||
        !getWheelAndLegNames(controller_nh, "rear_left_leg", leg_names) ||
        !getWheelAndLegNames(controller_nh, "rear_right_leg", leg_names)
        )
    {
      return false;
    }

    //wheel_joints_size_ = 4;

    if (wheel_names.size() != 4 && 
        leg_names.size() != 4 )
    {
      ROS_ERROR_STREAM_NAMED(name_,
          "#wheel (" << wheel_names.size() << ") != " <<
          "#legs (" << leg_names.size() << ").");
      return false;
    }

    else
    {
      wheel_joints_size_ = wheel_names.size();

      wheel_joints_.resize(wheel_joints_size_);
      leg_joints_.resize(wheel_joints_size_);
    }

    // Odometry related:
    double publish_rate;
    controller_nh.param("publish_rate", publish_rate, 50.0);
    ROS_INFO_STREAM_NAMED(name_, "Controller state will be published at "
                          << publish_rate << "Hz.");
    publish_period_ = ros::Duration(1.0 / publish_rate);

    controller_nh.param("open_loop", open_loop_, open_loop_);

    controller_nh.param("leg_desaxing_multiplier", leg_desaxing_multiplier_, leg_desaxing_multiplier_);
    ROS_INFO_STREAM_NAMED(name_, "leg desaxing will be multiplied by "
                          << leg_desaxing_multiplier_ << ".");

    if (controller_nh.hasParam("wheel_radius_multiplier"))
    {
      double wheel_radius_multiplier;
      controller_nh.getParam("wheel_radius_multiplier", wheel_radius_multiplier);

    }

    ROS_INFO_STREAM_NAMED(name_, "wheel radius will be multiplied by "
                          << wheel_radius_multiplier_ << ".");

    int velocity_rolling_window_size = 10;
    controller_nh.param("velocity_rolling_window_size", velocity_rolling_window_size, velocity_rolling_window_size);
    ROS_INFO_STREAM_NAMED(name_, "Velocity rolling window size of "
                          << velocity_rolling_window_size << ".");

    odometry_.setVelocityRollingWindowSize(velocity_rolling_window_size);

    // Twist command related:
    controller_nh.param("cmd_vel_timeout", cmd_vel_timeout_, cmd_vel_timeout_);
    ROS_INFO_STREAM_NAMED(name_, "Velocity commands will be considered old if they are older than "
                          << cmd_vel_timeout_ << "s.");

    controller_nh.param("allow_multiple_cmd_vel_publishers", allow_multiple_cmd_vel_publishers_, allow_multiple_cmd_vel_publishers_);
    ROS_INFO_STREAM_NAMED(name_, "Allow mutiple cmd_vel publishers is "
                          << (allow_multiple_cmd_vel_publishers_?"enabled":"disabled"));

    controller_nh.param("base_frame_id", base_frame_id_, base_frame_id_);
    ROS_INFO_STREAM_NAMED(name_, "Base frame_id set to " << base_frame_id_);

    controller_nh.param("odom_frame_id", odom_frame_id_, odom_frame_id_);
    ROS_INFO_STREAM_NAMED(name_, "Odometry frame_id set to " << odom_frame_id_);

    controller_nh.param("enable_odom_tf", enable_odom_tf_, enable_odom_tf_);
    ROS_INFO_STREAM_NAMED(name_, "Publishing to tf is " << (enable_odom_tf_?"enabled":"disabled"));

    // Velocity and acceleration limits:
    controller_nh.param("linear/x/has_velocity_limits"    , limiter_lin_x_.has_velocity_limits    , limiter_lin_x_.has_velocity_limits    );
    controller_nh.param("linear/x/has_acceleration_limits", limiter_lin_x_.has_acceleration_limits, limiter_lin_x_.has_acceleration_limits);
    controller_nh.param("linear/x/has_jerk_limits"        , limiter_lin_x_.has_jerk_limits        , limiter_lin_x_.has_jerk_limits        );
    controller_nh.param("linear/x/max_velocity"           , limiter_lin_x_.max_velocity           ,  limiter_lin_x_.max_velocity          );
    controller_nh.param("linear/x/min_velocity"           , limiter_lin_x_.min_velocity           , -limiter_lin_x_.max_velocity          );
    controller_nh.param("linear/x/max_acceleration"       , limiter_lin_x_.max_acceleration       ,  limiter_lin_x_.max_acceleration      );
    controller_nh.param("linear/x/min_acceleration"       , limiter_lin_x_.min_acceleration       , -limiter_lin_x_.max_acceleration      );
    controller_nh.param("linear/x/max_jerk"               , limiter_lin_x_.max_jerk               ,  limiter_lin_x_.max_jerk              );
    controller_nh.param("linear/x/min_jerk"               , limiter_lin_x_.min_jerk               , -limiter_lin_x_.max_jerk              );

    controller_nh.param("linear/y/has_velocity_limits"    , limiter_lin_y_.has_velocity_limits    , limiter_lin_y_.has_velocity_limits    );
    controller_nh.param("linear/y/has_acceleration_limits", limiter_lin_y_.has_acceleration_limits, limiter_lin_y_.has_acceleration_limits);
    controller_nh.param("linear/y/has_jerk_limits"        , limiter_lin_y_.has_jerk_limits        , limiter_lin_y_.has_jerk_limits        );
    controller_nh.param("linear/y/max_velocity"           , limiter_lin_y_.max_velocity           ,  limiter_lin_y_.max_velocity          );
    controller_nh.param("linear/y/min_velocity"           , limiter_lin_y_.min_velocity           , -limiter_lin_y_.max_velocity          );
    controller_nh.param("linear/y/max_acceleration"       , limiter_lin_y_.max_acceleration       ,  limiter_lin_y_.max_acceleration      );
    controller_nh.param("linear/y/min_acceleration"       , limiter_lin_y_.min_acceleration       , -limiter_lin_y_.max_acceleration      );
    controller_nh.param("linear/y/max_jerk"               , limiter_lin_y_.max_jerk               ,  limiter_lin_y_.max_jerk              );
    controller_nh.param("linear/y/min_jerk"               , limiter_lin_y_.min_jerk               , -limiter_lin_y_.max_jerk              );

    controller_nh.param("angular/z/has_velocity_limits"    , limiter_ang_.has_velocity_limits    , limiter_ang_.has_velocity_limits    );
    controller_nh.param("angular/z/has_acceleration_limits", limiter_ang_.has_acceleration_limits, limiter_ang_.has_acceleration_limits);
    controller_nh.param("angular/z/has_jerk_limits"        , limiter_ang_.has_jerk_limits        , limiter_ang_.has_jerk_limits        );
    controller_nh.param("angular/z/max_velocity"           , limiter_ang_.max_velocity           ,  limiter_ang_.max_velocity          );
    controller_nh.param("angular/z/min_velocity"           , limiter_ang_.min_velocity           , -limiter_ang_.max_velocity          );
    controller_nh.param("angular/z/max_acceleration"       , limiter_ang_.max_acceleration       ,  limiter_ang_.max_acceleration      );
    controller_nh.param("angular/z/min_acceleration"       , limiter_ang_.min_acceleration       , -limiter_ang_.max_acceleration      );
    controller_nh.param("angular/z/max_jerk"               , limiter_ang_.max_jerk               ,  limiter_ang_.max_jerk              );
    controller_nh.param("angular/z/min_jerk"               , limiter_ang_.min_jerk               , -limiter_ang_.max_jerk              );

    // Publish limited velocity:
    controller_nh.param("publish_cmd", publish_cmd_, publish_cmd_);

    // Publish wheel data:
    controller_nh.param("publish_wheel_joint_controller_state", publish_wheel_joint_controller_state_, publish_wheel_joint_controller_state_);

    // If either parameter is not available, we need to look up the value in the URDF
    bool lookup_leg_desaxing = !controller_nh.getParam("leg_desaxing", leg_desaxing_);
    bool lookup_wheel_radius = !controller_nh.getParam("wheel_radius", wheel_radius_);

    if (!setOdomParamsFromUrdf(root_nh,
                              wheel_names[0],
                              leg_names[0],
                              lookup_leg_desaxing,
                              lookup_wheel_radius))
    {
      return false;
    }

    // Regardless of how we got the separation and radius, use them
    // to set the odometry parameters
    const double ld  = leg_desaxing_multiplier_   * leg_desaxing_;
    const double wr = wheel_radius_multiplier_  * wheel_radius_;
    odometry_.setWheelParams(ld, wr, leg_angle_, leg_ratio_);
    ROS_INFO_STREAM_NAMED(name_,
                          "Odometry params : leg_desaxing " << ld
                          << ", wheel radius " << wr
                          << ", leg angle " << leg_angle_);

    if (publish_cmd_)
    {
      cmd_vel_pub_.reset(new realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>(controller_nh, "cmd_vel_out", 100));
    }

    // Wheel joint controller state:
    if (publish_wheel_joint_controller_state_)
    {
      controller_state_pub_.reset(new realtime_tools::RealtimePublisher<control_msgs::JointTrajectoryControllerState>(controller_nh, "wheel_joint_controller_state", 100));

      const size_t num_wheel = wheel_joints_size_ * 2; // for wheel and legs

      controller_state_pub_->msg_.joint_names.resize(num_wheel);

      controller_state_pub_->msg_.desired.positions.resize(num_wheel);
      controller_state_pub_->msg_.desired.velocities.resize(num_wheel);
      controller_state_pub_->msg_.desired.accelerations.resize(num_wheel);
      controller_state_pub_->msg_.desired.effort.resize(num_wheel);

      controller_state_pub_->msg_.actual.positions.resize(num_wheel);
      controller_state_pub_->msg_.actual.velocities.resize(num_wheel);
      controller_state_pub_->msg_.actual.accelerations.resize(num_wheel);
      controller_state_pub_->msg_.actual.effort.resize(num_wheel);

      controller_state_pub_->msg_.error.positions.resize(num_wheel);
      controller_state_pub_->msg_.error.velocities.resize(num_wheel);
      controller_state_pub_->msg_.error.accelerations.resize(num_wheel);
      controller_state_pub_->msg_.error.effort.resize(num_wheel);

      for (size_t i = 0; i < wheel_joints_size_; ++i)
      {
        controller_state_pub_->msg_.joint_names[i] = wheel_names[i];
        controller_state_pub_->msg_.joint_names[i] = leg_names[i];
      }

      wheel_vel_previous_.resize(wheel_joints_size_, 0.0);
      //leg_vel_previous_.resize(wheel_joints_size_, 0.0);
    }

    setOdomPubFields(root_nh, controller_nh);

    // Get the joint object to use in the realtime loop
    for (size_t i = 0; i < wheel_joints_size_; ++i)
    {
      ROS_INFO_STREAM_NAMED(name_,
                            "Adding wheel with joint name: " << wheel_names[i]
                            << " and leg with joint name: " << leg_names[i]);
      wheel_joints_[i] = vel_joint_if->getHandle(wheel_names[i]);  // throws on failure
      leg_joints_[i] = pos_joint_if->getHandle(leg_names[i]);  // throws on failure
    }

    sub_command_ = controller_nh.subscribe("cmd_vel", 1, &HolonomicController::cmdVelCallback, this);

    // Initialize dynamic parameters
    DynamicParams dynamic_params;
    dynamic_params.wheel_radius_multiplier  = wheel_radius_multiplier_;
    dynamic_params.leg_desaxing_multiplier   = leg_desaxing_multiplier_;

    dynamic_params.publish_rate = publish_rate;
    dynamic_params.enable_odom_tf = enable_odom_tf_;

    dynamic_params_.writeFromNonRT(dynamic_params);

    // Initialize dynamic_reconfigure server
    HolonomicControllerConfig config;
    config.wheel_radius_multiplier  = wheel_radius_multiplier_;
    config.leg_desaxing_multiplier   = leg_desaxing_multiplier_;

    config.publish_rate = publish_rate;
    config.enable_odom_tf = enable_odom_tf_;

    dyn_reconf_server_ = std::make_shared<ReconfigureServer>(dyn_reconf_server_mutex_, controller_nh);

    // Update parameters
    dyn_reconf_server_mutex_.lock();
    dyn_reconf_server_->updateConfig(config);
    dyn_reconf_server_mutex_.unlock();

    dyn_reconf_server_->setCallback(boost::bind(&HolonomicController::reconfCallback, this, _1, _2));

    return true;
  }

  void HolonomicController::update(const ros::Time& time, const ros::Duration& period)
  {
    // update parameter from dynamic reconf
    updateDynamicParams();

    // Apply (possibly new) multipliers:
    const double ld  = leg_desaxing_multiplier_   *leg_desaxing_;
    const double wr = wheel_radius_multiplier_  * wheel_radius_;
    const double la = leg_angle_;

    odometry_.setWheelParams(ld, wr, la, leg_ratio_);
    // COMPUTE AND PUBLISH ODOMETRY
    if (open_loop_)
    {
      ROS_ERROR("OPEN LOOP NOT IMPLEMENTED");
      //odometry_.updateOpenLoop(last0_cmd_.lin_x, last0_cmd_.lin_y, last0_cmd_.ang, time);
    }
    else
    {
      double wheel_pos[4]  = {0.0, 0.0, 0.0, 0.0};
      double leg_pos[4]  = {0.0, 0.0, 0.0, 0.0};
      for (size_t i = 0; i < wheel_joints_size_; ++i)
      {
        const double wp = wheel_joints_[i].getPosition();
        const double lp = leg_joints_[i].getPosition();
        if (std::isnan(wp) || std::isnan(lp))
          return;

        wheel_pos[i] = wp;
        leg_pos[i] = lp;
      }

      // Estimate linear and angular velocity using joint information
      odometry_.update(wheel_pos, leg_pos, time);
    }

    // Publish odometry message
    if (last_state_publish_time_ + publish_period_ < time)
    {
      last_state_publish_time_ += publish_period_;
      // Compute and store orientation info
      const geometry_msgs::Quaternion orientation(
            tf::createQuaternionMsgFromYaw(odometry_.getHeading()));

      // Populate odom message and publish
      if (odom_pub_->trylock())
      {
        odom_pub_->msg_.header.stamp = time;
        odom_pub_->msg_.pose.pose.position.x = odometry_.getX();
        odom_pub_->msg_.pose.pose.position.y = odometry_.getY();
        odom_pub_->msg_.pose.pose.orientation = orientation;
        odom_pub_->msg_.twist.twist.linear.x  = odometry_.getLinearX();
        odom_pub_->msg_.twist.twist.linear.y  = odometry_.getLinearY();
        odom_pub_->msg_.twist.twist.angular.z = odometry_.getAngular();
        odom_pub_->unlockAndPublish();
      }

      // Publish tf /odom frame
      if (enable_odom_tf_ && tf_odom_pub_->trylock())
      {
        geometry_msgs::TransformStamped& odom_frame = tf_odom_pub_->msg_.transforms[0];
        odom_frame.header.stamp = time;
        odom_frame.transform.translation.x = odometry_.getX();
        odom_frame.transform.translation.y = odometry_.getY();
        odom_frame.transform.rotation = orientation;
        tf_odom_pub_->unlockAndPublish();
      }
    }


    // MOVE ROBOT
    // Retreive current velocity command and time step:
    Commands curr_cmd = *(command_.readFromRT());
    const double dt = (time - curr_cmd.stamp).toSec();

    // Brake if cmd_vel has timeout:
    if (dt > cmd_vel_timeout_)
    {
      curr_cmd.lin_x = 0.0;
      curr_cmd.lin_y = 0.0;
      curr_cmd.ang = 0.0;
    }

    // Limit velocities and accelerations:
    const double cmd_dt(period.toSec());

    limiter_lin_x_.limit(curr_cmd.lin_x, last0_cmd_.lin_x, last1_cmd_.lin_x, cmd_dt);
    limiter_lin_y_.limit(curr_cmd.lin_y, last0_cmd_.lin_y, last1_cmd_.lin_y, cmd_dt);
    limiter_ang_.limit(curr_cmd.ang, last0_cmd_.ang, last1_cmd_.ang, cmd_dt);

    last1_cmd_ = last0_cmd_;
    last0_cmd_ = curr_cmd;

    // Publish limited velocity:
    if (publish_cmd_ && cmd_vel_pub_ && cmd_vel_pub_->trylock())
    {
      cmd_vel_pub_->msg_.header.stamp = time;
      cmd_vel_pub_->msg_.twist.linear.x = curr_cmd.lin_x;
      cmd_vel_pub_->msg_.twist.linear.y = curr_cmd.lin_y;
      cmd_vel_pub_->msg_.twist.angular.z = curr_cmd.ang;
      cmd_vel_pub_->unlockAndPublish();
    }

    // Compute wheel velocities and leg positions:
    double wheel_vel(0.0);
    double leg_pos(0.0);

    const double Vzx = curr_cmd.ang * ld * sin(la);
    const double Vzy = curr_cmd.ang * ld * cos(la);

    double Vx, Vy;

    for (int i = 0; i < 4; ++i) {
      if (i == 0) {
        Vx = curr_cmd.lin_x - Vzx;
        Vy = curr_cmd.lin_y + Vzy;
      }
      else if (i == 1) {
        Vx = curr_cmd.lin_x + Vzx;
        Vy = curr_cmd.lin_y + Vzy;
      }
      else if (i == 2) {
        Vx = curr_cmd.lin_x - Vzx;
        Vy = curr_cmd.lin_y - Vzy;
      }
      else {
        Vx = curr_cmd.lin_x + Vzx;
        Vy = curr_cmd.lin_y - Vzy;
      }

      wheel_vel = std::sqrt(std::pow(Vx,2) + std::pow(Vy,2));
      if (fabs(wheel_vel) < 1e-6) {leg_pos = 0.0;}
      else if (Vy < 0.0) { leg_pos = -std::acos( Vx/wheel_vel );}
      else { leg_pos = std::acos( Vx/wheel_vel );}

      // Set wheel velocities:
      wheel_joints_[i].setCommand(wheel_vel/wr);
      leg_joints_[i].setCommand(leg_pos);
    }

    publishWheelAndLegData(time, period, curr_cmd, ld, wr, la);
    time_previous_ = time;

  }

  void HolonomicController::starting(const ros::Time& time)
  {
    brake();

    // Register starting time used to keep fixed rate
    last_state_publish_time_ = time;
    time_previous_ = time;

    odometry_.init(time);
  }

  void HolonomicController::stopping(const ros::Time& /*time*/)
  {
    brake();
  }

  void HolonomicController::brake()
  {
    const double vel = 0.0;
    const double pos = 0.0;
    for (size_t i = 0; i < wheel_joints_size_; ++i)
    {
      wheel_joints_[i].setCommand(vel);
      leg_joints_[i].setCommand(pos);
    }
  }

  void HolonomicController::cmdVelCallback(const geometry_msgs::Twist& command)
  {
    if (isRunning())
    {
      // check that we don't have multiple publishers on the command topic
      if (!allow_multiple_cmd_vel_publishers_ && sub_command_.getNumPublishers() > 1)
      {
        ROS_ERROR_STREAM_THROTTLE_NAMED(1.0, name_, "Detected " << sub_command_.getNumPublishers()
            << " publishers. Only 1 publisher is allowed. Going to brake.");
        brake();
        return;
      }

      if(!std::isfinite(command.angular.z) || !std::isfinite(command.angular.x))
      {
        ROS_WARN_THROTTLE(1.0, "Received NaN in velocity command. Ignoring.");
        return;
      }

      command_struct_.ang   = command.angular.z;
      command_struct_.lin_x   = command.linear.x;
      command_struct_.lin_y   = command.linear.y;
      command_struct_.stamp = ros::Time::now();
      command_.writeFromNonRT (command_struct_);
      ROS_DEBUG_STREAM_NAMED(name_,
                             "Added values to command. "
                             << "Ang: "   << command_struct_.ang << ", "
                             << "Lin X: "   << command_struct_.lin_x << ", "
                             << "Lin Y: "   << command_struct_.lin_y << ", "
                             << "Stamp: " << command_struct_.stamp);
    }
    else
    {
      ROS_ERROR_NAMED(name_, "Can't accept new commands. Controller is not running.");
    }
  }

  bool HolonomicController::getWheelAndLegNames(ros::NodeHandle& controller_nh,
                              const std::string& wheel_param,
                              std::vector<std::string>& wheel_names)
  {
      XmlRpc::XmlRpcValue wheel_list;
      if (!controller_nh.getParam(wheel_param, wheel_list))
      {
        ROS_ERROR_STREAM_NAMED(name_,
            "Couldn't retrieve wheel param '" << wheel_param << "'.");
        return false;
      }

      if (wheel_list.getType() == XmlRpc::XmlRpcValue::TypeArray)
      {
        if (wheel_list.size() == 0)
        {
          ROS_ERROR_STREAM_NAMED(name_,
              "Wheel param '" << wheel_param << "' is an empty list");
          return false;
        }

        for (int i = 0; i < wheel_list.size(); ++i)
        {
          if (wheel_list[i].getType() != XmlRpc::XmlRpcValue::TypeString)
          {
            ROS_ERROR_STREAM_NAMED(name_,
                "Wheel param '" << wheel_param << "' #" << i <<
                " isn't a string.");
            return false;
          }
        }

        wheel_names.resize(wheel_list.size());
        for (int i = 0; i < wheel_list.size(); ++i)
        {
          wheel_names[i] = static_cast<std::string>(wheel_list[i]);
        }
      }
      else if (wheel_list.getType() == XmlRpc::XmlRpcValue::TypeString)
      {
        wheel_names.push_back(wheel_list);
      }
      else
      {
        ROS_ERROR_STREAM_NAMED(name_,
            "Wheel param '" << wheel_param <<
            "' is neither a list of strings nor a string.");
        return false;
      }

      return true;
  }

  bool HolonomicController::setOdomParamsFromUrdf(ros::NodeHandle& root_nh,
                             const std::string& wheel_name,
                             const std::string& leg_name,
                             bool lookup_leg_desaxing,
                             bool lookup_wheel_radius)
  {
    if (!(lookup_leg_desaxing || lookup_wheel_radius))
    {
      // Short-circuit in case we don't need to look up anything, so we don't have to parse the URDF
      return true;
    }

    // Parse robot description
    const std::string model_param_name = "robot_description";
    bool res = root_nh.hasParam(model_param_name);
    std::string robot_model_str="";
    if (!res || !root_nh.getParam(model_param_name,robot_model_str))
    {
      ROS_ERROR_NAMED(name_, "Robot description couldn't be retrieved from param server.");
      return false;
    }

    urdf::ModelInterfaceSharedPtr model(urdf::parseURDF(robot_model_str));

    urdf::JointConstSharedPtr wheel_joint(model->getJoint(wheel_name));
    urdf::JointConstSharedPtr leg_joint(model->getJoint(leg_name));

    if (!wheel_joint)
    {
      ROS_ERROR_STREAM_NAMED(name_, wheel_name
                             << " couldn't be retrieved from model description");
      return false;
    }

    if (!leg_joint)
    {
      ROS_ERROR_STREAM_NAMED(name_, leg_name
                             << " couldn't be retrieved from model description");
      return false;
    }

    if (lookup_leg_desaxing)
    {
      // Get leg desaxing
      std::string parent_name = leg_joint->parent_link_name + "_joint"; // TODO : better to use Link method or param
      double leg_pos_x(0.0), leg_pos_y(0.0);
      while ( parent_name.compare("base_link") != 0) {
        urdf::JointConstSharedPtr parent_joint(model->getJoint(parent_name));
        ROS_WARN("Parent of the link '%s' is not 'base_link' but '%s'", leg_joint->child_link_name.c_str(), parent_joint->child_link_name.c_str());
        ROS_WARN("We'll try to go up to 'base_link'");
        leg_pos_x += parent_joint->parent_to_joint_origin_transform.position.x;
        leg_pos_y += parent_joint->parent_to_joint_origin_transform.position.y;
        parent_name = parent_joint->parent_link_name;
      };
      leg_pos_x += leg_joint->parent_to_joint_origin_transform.position.x;
      leg_pos_y += leg_joint->parent_to_joint_origin_transform.position.y;

      ROS_INFO_STREAM("leg to origin: " << leg_pos_x << ","
                      << leg_pos_y);

      leg_desaxing_ = std::sqrt(std::pow(leg_pos_x,2)+std::pow(leg_pos_y,2));
      leg_ratio_ = 2.0*fabs(leg_pos_y) / leg_desaxing_;
      leg_angle_ = atan(leg_pos_y / leg_pos_x);

    }

    if (lookup_wheel_radius)
    {
      // Get wheel radius
      if (!getWheelRadius(model->getLink(wheel_joint->child_link_name), wheel_radius_))
      {
        ROS_ERROR_STREAM_NAMED(name_, "Couldn't retrieve " << wheel_name << " wheel radius");
        return false;
      }
    }

    return true;
  }

  void HolonomicController::setOdomPubFields(ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
  {
    // Get and check params for covariances
    XmlRpc::XmlRpcValue pose_cov_list;
    controller_nh.getParam("pose_covariance_diagonal", pose_cov_list);
    ROS_ASSERT(pose_cov_list.getType() == XmlRpc::XmlRpcValue::TypeArray);
    ROS_ASSERT(pose_cov_list.size() == 6);
    for (int i = 0; i < pose_cov_list.size(); ++i)
      ROS_ASSERT(pose_cov_list[i].getType() == XmlRpc::XmlRpcValue::TypeDouble);

    XmlRpc::XmlRpcValue twist_cov_list;
    controller_nh.getParam("twist_covariance_diagonal", twist_cov_list);
    ROS_ASSERT(twist_cov_list.getType() == XmlRpc::XmlRpcValue::TypeArray);
    ROS_ASSERT(twist_cov_list.size() == 6);
    for (int i = 0; i < twist_cov_list.size(); ++i)
      ROS_ASSERT(twist_cov_list[i].getType() == XmlRpc::XmlRpcValue::TypeDouble);

    // Setup odometry realtime publisher + odom message constant fields
    odom_pub_.reset(new realtime_tools::RealtimePublisher<nav_msgs::Odometry>(controller_nh, "odom", 100));
    odom_pub_->msg_.header.frame_id = odom_frame_id_;
    odom_pub_->msg_.child_frame_id = base_frame_id_;
    odom_pub_->msg_.pose.pose.position.z = 0;
    odom_pub_->msg_.pose.covariance = {
        static_cast<double>(pose_cov_list[0]), 0., 0., 0., 0., 0.,
        0., static_cast<double>(pose_cov_list[1]), 0., 0., 0., 0.,
        0., 0., static_cast<double>(pose_cov_list[2]), 0., 0., 0.,
        0., 0., 0., static_cast<double>(pose_cov_list[3]), 0., 0.,
        0., 0., 0., 0., static_cast<double>(pose_cov_list[4]), 0.,
        0., 0., 0., 0., 0., static_cast<double>(pose_cov_list[5]) };
    odom_pub_->msg_.twist.twist.linear.y  = 0;
    odom_pub_->msg_.twist.twist.linear.z  = 0;
    odom_pub_->msg_.twist.twist.angular.x = 0;
    odom_pub_->msg_.twist.twist.angular.y = 0;
    odom_pub_->msg_.twist.covariance = {
        static_cast<double>(twist_cov_list[0]), 0., 0., 0., 0., 0.,
        0., static_cast<double>(twist_cov_list[1]), 0., 0., 0., 0.,
        0., 0., static_cast<double>(twist_cov_list[2]), 0., 0., 0.,
        0., 0., 0., static_cast<double>(twist_cov_list[3]), 0., 0.,
        0., 0., 0., 0., static_cast<double>(twist_cov_list[4]), 0.,
        0., 0., 0., 0., 0., static_cast<double>(twist_cov_list[5]) };
    tf_odom_pub_.reset(new realtime_tools::RealtimePublisher<tf::tfMessage>(root_nh, "/tf", 100));
    tf_odom_pub_->msg_.transforms.resize(1);
    tf_odom_pub_->msg_.transforms[0].transform.translation.z = 0.0;
    tf_odom_pub_->msg_.transforms[0].child_frame_id = base_frame_id_;
    tf_odom_pub_->msg_.transforms[0].header.frame_id = odom_frame_id_;
  }

  void HolonomicController::reconfCallback(HolonomicControllerConfig& config, uint32_t /*level*/)
  {
    DynamicParams dynamic_params;
    dynamic_params.wheel_radius_multiplier  = config.wheel_radius_multiplier;
    dynamic_params.leg_desaxing_multiplier   = config.leg_desaxing_multiplier;

    dynamic_params.publish_rate = config.publish_rate;

    dynamic_params.enable_odom_tf = config.enable_odom_tf;

    dynamic_params_.writeFromNonRT(dynamic_params);

    ROS_INFO_STREAM_NAMED(name_, "Dynamic Reconfigure:\n" << dynamic_params);
  }

  void HolonomicController::updateDynamicParams()
  {
    // Retreive dynamic params:
    const DynamicParams dynamic_params = *(dynamic_params_.readFromRT());

    wheel_radius_multiplier_  = dynamic_params.wheel_radius_multiplier;
    leg_desaxing_multiplier_   = dynamic_params.leg_desaxing_multiplier;

    publish_period_ = ros::Duration(1.0 / dynamic_params.publish_rate);
    enable_odom_tf_ = dynamic_params.enable_odom_tf;
  }

  void HolonomicController::publishWheelAndLegData(const ros::Time& time, const ros::Duration& period, Commands& curr_cmd,
          double leg_desaxing, double wheel_radius, double leg_angle)
  {
    if (publish_wheel_joint_controller_state_ && controller_state_pub_->trylock())
    {
      const double cmd_dt(period.toSec());

      // Compute desired wheel velocities, that is before applying limits:
      const double Vzx = curr_cmd.ang * leg_desaxing * sin(leg_angle);
      const double Vzy = curr_cmd.ang * leg_desaxing * cos(leg_angle);
      
      double Vx, Vy, V, leg_pos_desired;

      controller_state_pub_->msg_.header.stamp = time;

      for (size_t i = 0; i < wheel_joints_size_; ++i) {
        if (i == 0) {
          Vx = curr_cmd.lin_x - Vzx;
          Vy = curr_cmd.lin_y + Vzy;
        }
        else if (i == 1) {
          Vx = curr_cmd.lin_x + Vzx;
          Vy = curr_cmd.lin_y + Vzy;
        }
        else if (i == 2) {
          Vx = curr_cmd.lin_x - Vzx;
          Vy = curr_cmd.lin_y - Vzy;
        }
        else {
          Vx = curr_cmd.lin_x + Vzx;
          Vy = curr_cmd.lin_y - Vzy;
        }
        V = std::sqrt(std::pow(Vx,2) + std::pow(Vy,2));
        if (V == 0.0) {leg_pos_desired = 0.0;}
        else if (Vy < 0.0) { leg_pos_desired = -std::acos( Vx/V );}
        else { leg_pos_desired = std::acos( Vx/V );}

        const double wheel_vel_desired  = V / wheel_radius;

        const double control_duration = (time - time_previous_).toSec();

        const double wheel_acc = (wheel_joints_[i].getVelocity() - wheel_vel_previous_[i]) / control_duration;
        const double leg_acc = (leg_joints_[i].getVelocity() - leg_vel_previous_[i]) / control_duration;
   
        // Actual
        controller_state_pub_->msg_.actual.positions[i]     = wheel_joints_[i].getPosition();
        controller_state_pub_->msg_.actual.velocities[i]    = wheel_joints_[i].getVelocity();
        controller_state_pub_->msg_.actual.accelerations[i] = wheel_acc;
        controller_state_pub_->msg_.actual.effort[i]        = wheel_joints_[i].getEffort();

        controller_state_pub_->msg_.actual.positions[i + wheel_joints_size_]     = leg_joints_[i].getPosition();
        controller_state_pub_->msg_.actual.velocities[i + wheel_joints_size_]    = leg_joints_[i].getVelocity();
        controller_state_pub_->msg_.actual.accelerations[i + wheel_joints_size_] = leg_acc;
        controller_state_pub_->msg_.actual.effort[i+ wheel_joints_size_]         = leg_joints_[i].getEffort();

        // Desired
        controller_state_pub_->msg_.desired.positions[i]    += wheel_vel_desired * cmd_dt;
        controller_state_pub_->msg_.desired.velocities[i]    = wheel_vel_desired;
        controller_state_pub_->msg_.desired.accelerations[i] = (wheel_vel_desired - wheel_vel_desired_previous_[i]) * cmd_dt;
        controller_state_pub_->msg_.desired.effort[i]        = std::numeric_limits<double>::quiet_NaN();

        controller_state_pub_->msg_.desired.positions[i + wheel_joints_size_]    += leg_pos_desired;
        controller_state_pub_->msg_.desired.velocities[i + wheel_joints_size_]    = std::numeric_limits<double>::quiet_NaN(); // TO BE MODIFY TO GET VELOCITY AND ACCEL CONTROL
        controller_state_pub_->msg_.desired.accelerations[i + wheel_joints_size_] = std::numeric_limits<double>::quiet_NaN(); // TO BE MODIFY TO GET VELOCITY AND ACCEL CONTROL
        controller_state_pub_->msg_.desired.effort[i+ wheel_joints_size_]         = std::numeric_limits<double>::quiet_NaN();

        // Error
        controller_state_pub_->msg_.error.positions[i]     = controller_state_pub_->msg_.desired.positions[i] -
                                                                              controller_state_pub_->msg_.actual.positions[i];
        controller_state_pub_->msg_.error.velocities[i]    = controller_state_pub_->msg_.desired.velocities[i] -
                                                                              controller_state_pub_->msg_.actual.velocities[i];
        controller_state_pub_->msg_.error.accelerations[i] = controller_state_pub_->msg_.desired.accelerations[i] -
                                                                              controller_state_pub_->msg_.actual.accelerations[i];
        controller_state_pub_->msg_.error.effort[i]        = controller_state_pub_->msg_.desired.effort[i] -
                                                                              controller_state_pub_->msg_.actual.effort[i];

        controller_state_pub_->msg_.error.positions[i + wheel_joints_size_]     = controller_state_pub_->msg_.desired.positions[i + wheel_joints_size_] -
                                                                                                   controller_state_pub_->msg_.actual.positions[i + wheel_joints_size_];
        controller_state_pub_->msg_.error.velocities[i + wheel_joints_size_]    = controller_state_pub_->msg_.desired.velocities[i + wheel_joints_size_] -
                                                                                                   controller_state_pub_->msg_.actual.velocities[i + wheel_joints_size_];
        controller_state_pub_->msg_.error.accelerations[i + wheel_joints_size_] = controller_state_pub_->msg_.desired.accelerations[i + wheel_joints_size_] -
                                                                                                   controller_state_pub_->msg_.actual.accelerations[i + wheel_joints_size_];
        controller_state_pub_->msg_.error.effort[i+ wheel_joints_size_]         = controller_state_pub_->msg_.desired.effort[i + wheel_joints_size_] -
                                                                                                   controller_state_pub_->msg_.actual.effort[i + wheel_joints_size_];

        // Save previous velocities to compute acceleration
        wheel_vel_previous_[i] = wheel_joints_[i].getVelocity();
        wheel_vel_desired_previous_[i] = wheel_vel_desired;
        leg_vel_previous_[i] = leg_joints_[i].getVelocity();
      }

      controller_state_pub_->unlockAndPublish();
    }
  }

} // namespace diff_drive_controller

PLUGINLIB_EXPORT_CLASS(holonomic_controller::HolonomicController, controller_interface::ControllerBase);
