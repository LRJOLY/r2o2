#include <holonomic_controller/odometry.h>

#include <boost/bind.hpp>

namespace holonomic_controller
{
  namespace bacc = boost::accumulators;

  Odometry::Odometry(size_t velocity_rolling_window_size)
  : timestamp_(0.0)
  , x_(0.0)
  , y_(0.0)
  , heading_(0.0)
  , linear_x_(0.0)
  , linear_y_(0.0)
  , angular_(0.0)
  , leg_desaxing_(0.0)
  , wheel_radius_(0.0)
  , wheel_old_pos_{0.0}
  , velocity_rolling_window_size_(velocity_rolling_window_size)
  , linear_acc_x_(RollingWindow::window_size = velocity_rolling_window_size)
  , linear_acc_y_(RollingWindow::window_size = velocity_rolling_window_size)
  , angular_acc_(RollingWindow::window_size = velocity_rolling_window_size)
  , integrate_fun_(boost::bind(&Odometry::integrateExact, this, _1, _2, _3))
  {
  }

  void Odometry::init(const ros::Time& time)
  {
    // Reset accumulators and timestamp:
    resetAccumulators();
    timestamp_ = time;
  }

  bool Odometry::update(double wheel_pos[4], double leg_pos[4], const ros::Time &time)
  {
    double wheel_cur_pos;
    double wheel_est_vel;
    double linear_x(0.0);
    double linear_y(0.0);
    double angular(0.0);
    for (int i=0; i <4 ; ++i) {
        /// Get current wheel joint positions:
        wheel_cur_pos = wheel_radius_ * wheel_pos[i];
        /// Estimate velocity of wheels using old and current position:
        wheel_est_vel  = wheel_cur_pos  - wheel_old_pos_[i];
        /// Update old position with current:
        wheel_old_pos_[i]  = wheel_cur_pos;
        /// Calculate wheel velocity in robot frame
        linear_x += wheel_est_vel*cos(leg_pos[i]);
        linear_y += wheel_est_vel*sin(leg_pos[i]);
        angular += std::pow(-1,i)*wheel_est_vel*cos(leg_pos[i])/(leg_desaxing_*leg_ratio_);
    }
    linear_x /= 4.0;
    linear_y /= 4.0;
    angular /= 4.0;

    /// Integrate odometry:
    integrate_fun_(linear_x, linear_y, angular);

    /// We cannot estimate the speed with very small time intervals:
    const double dt = (time - timestamp_).toSec();
    if (dt < 0.0001)
      return false; // Interval too small to integrate with

    timestamp_ = time;

    /// Estimate speeds using a rolling mean to filter them out:
    linear_acc_x_(linear_x/dt);
    linear_acc_y_(linear_y/dt);
    angular_acc_(angular/dt);

    linear_x_ = bacc::rolling_mean(linear_acc_x_);
    linear_y_ = bacc::rolling_mean(linear_acc_y_);
    angular_ = bacc::rolling_mean(angular_acc_);

    return true;
  }

  void Odometry::updateOpenLoop(double linear_x, double linear_y, double angular, const ros::Time &time)
  {
    /// Save last linear and angular velocity:
    linear_x_ = linear_x;
    linear_y_ = linear_y;
    angular_ = angular;

    /// Integrate odometry:
    const double dt = (time - timestamp_).toSec();
    timestamp_ = time;
    integrate_fun_(linear_x * dt, linear_y * dt, angular * dt); // TODO : until now linear_y is not taken into account
  }

  void Odometry::setWheelParams(double leg_desaxing, double wheel_radius, double leg_angle, double leg_ratio)
  {
    leg_desaxing_   = leg_desaxing;
    wheel_radius_  = wheel_radius;
    leg_angle_ = leg_angle;
    leg_ratio_ = leg_ratio;
  }

  void Odometry::setVelocityRollingWindowSize(size_t velocity_rolling_window_size)
  {
    velocity_rolling_window_size_ = velocity_rolling_window_size;

    resetAccumulators();
  }

  void Odometry::integrateRungeKutta2(double linear_x, double linear_y, double angular)
  {
    const double direction = heading_ + angular * 0.5;

    /// Runge-Kutta 2nd order integration:
    x_       += linear_x*cos(direction) - linear_y*sin(direction);
    y_       += linear_x*sin(direction) + linear_y*cos(direction);
    heading_ += angular;
  }

  /**
   * \brief Other possible integration method provided by the class
   * \param linear_x
   * \param linear_y
   * \param angular
   */
  void Odometry::integrateExact(double linear_x, double linear_y, double angular)
  {
    if (fabs(angular) < 1e-6 or fabs(linear_y) > 1e-6)
      integrateRungeKutta2(linear_x, linear_y, angular);
    else
    {
      /// Exact integration (should solve problems when angular is zero):
      const double heading_old = heading_;
      const double r = linear_x/angular;
      heading_ += angular;
      x_       +=  r * (sin(heading_) - sin(heading_old));
      y_       += -r * (cos(heading_) - cos(heading_old));
    }
  }

  void Odometry::resetAccumulators()
  {
    linear_acc_x_ = RollingMeanAcc(RollingWindow::window_size = velocity_rolling_window_size_);
    linear_acc_y_ = RollingMeanAcc(RollingWindow::window_size = velocity_rolling_window_size_);
    angular_acc_ = RollingMeanAcc(RollingWindow::window_size = velocity_rolling_window_size_);
  }

} // namespace holonomic_controller


